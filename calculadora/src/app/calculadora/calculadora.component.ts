import { Component } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent {
  titulo = 'Aplicacion Calculadora';
  resultado ='';
  valorA = '';
  valorB = '';
  sumar(){
    this.resultado = this.valorA + this.valorB;
  }
}
